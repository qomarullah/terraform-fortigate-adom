# Fortigate Manager with Terraform

## Preparation
There is 3 type resources in this repo please rename to main.tf before use it
- main.tf.object 
- main.tf.policy 
- main.tf.pwdpolicy 

Then create terraform.auto.tfvars for credential or you can use variable input

```sh
#credential
hostname     = "xxxxxxxx:xxxxx"
username     = "xxxx"
password     = "xxxx"
```


## How to run

```sh
terraform plan
then 
./apply.sh {adom_name} # this will trigger force unlock if condition is locked due to previous error

```
