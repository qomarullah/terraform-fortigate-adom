#!/bin/bash

#unlock
file="presession.txt"
adom_name=$1

if [ "$adom_name" == "" ]; then

    echo "parameter ./apply.sh {adom_name}"
    echo "example:"
    echo "./apply.sh terraform2"

else

    presession=`awk 1 ORS='' $file `
    echo $presession
    cd unlock
    terraform apply -var="presession=$presession" -var="adom_name=$adom_name" --auto-approve
    cd ../

    #apply
    dt=$(date '+%m%d%H%M%S');
    cp presession.txt presession.txt.$dt
    terraform apply -var="adom_name=$adom_name" --auto-approve

fi