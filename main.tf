terraform {
  required_providers {
    fortimanager = {
      source = "fortinetdev/fortimanager"
      version = "1.3.5"
    }
  }
  backend "local" {
    path = "state/terraform.tfstate"
  }
}

provider "fortimanager" {
  hostname     = "${var.hostname}"
  username     = "${var.username}"
  password     = "${var.password}"
  insecure     = "true"
  scopetype    = "adom"
  adom         = "${var.adom_name}"
  logsession = true
  presession = ""
}


resource "fortimanager_exec_workspace_action" "lockres" { # lock root ADOM
  scopetype      = "inherit"
  action         = "lockbegin"
  target         = ""
  param          = ""
  force_recreate = uuid()
  comment        = ""
}


resource "fortimanager_object_user_passwordpolicy" "passwordpolicy" {
  expire_days              = 90
  #invalid when value exist disable/enable 
  expired_password_renewal = ""  
  name                     = "passwordpolicy"
  warn_days                = 15
  depends_on     = [fortimanager_exec_workspace_action.lockres]
 
}


resource "fortimanager_exec_workspace_action" "unlockres" { # save change and unlock root ADOM
  scopetype      = "inherit"
  action         = "lockend"
  target         = ""
  param          = ""
  force_recreate = uuid()
  comment        = ""
  depends_on     = [fortimanager_object_user_passwordpolicy.passwordpolicy]
}