#!/bin/bash
adom_name=$1
pressision_file=$2

if [ "$adom_name" == "" ] || [ "$pressision_file" == "" ]; then

    echo "parameter ./unlock.sh {adom_name} {filename pressision}"
    echo "example:"
    echo "./unlock.sh terraform2 pression.txt"

else 
    presession=`awk 1 ORS='' $2 `
    echo $presession
    cd unlock
    terraform apply -var="presession=$presession" -var="adom_name=$adom_name" --auto-approve
    cd ../

fi 