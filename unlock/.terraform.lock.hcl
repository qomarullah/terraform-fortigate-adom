# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/fortinetdev/fortimanager" {
  version     = "1.3.4"
  constraints = "1.3.4"
  hashes = [
    "h1:VNbWu1IlP6JObNCB/DjtgnpbJeISMhmtQIOKbb/K+BE=",
    "zh:2ab7135c762da305c42741e676d0a34fbddf4df4b8f3648338e09ecf4d4b578a",
    "zh:55eda01e6e00749626c4f85b387415ebf1aefffd2c7169b91071ea8bba3cedf2",
    "zh:5c70cd3c54fe8923b9035a6b9ebcb40fe0cc0835c65576207c270ab9c76040c2",
    "zh:69204bc439d72d7b40b15c34543f95a5856f82fd997df1b0e17ee5e24e9d0b35",
    "zh:755a4dac53c65d30c25eae2b696299e149b394ac06eb27528d9cf580a58209de",
    "zh:7bbdd11da8b1dd9b3a050364e2dfe026c6c6911cf216d1be1baa594f8d04e5ce",
    "zh:af7352f848467f339b23cb9c886a1f2907f06875fedee0bf380ae5ad81e4c7ca",
    "zh:c28c101f6847720e084bd6297ca6f014d4f90cef8d64974ccf0773ea0d42abaa",
    "zh:df9ca215257c1659817d18bced386df1f80b86db70f72bcd03cc27af2fff10e4",
    "zh:e64daccd1b1bdc24e8b50ed16011b9fd944ee8312189b408cbce1db6daae9c8f",
    "zh:ed5e86942c386adb4877a896275e0fd1417af5830905dcaf77c542d4c3da2d6d",
    "zh:fef6ad2f1a12ba8529a50f0976a566d1f14bbe0f5d1c1879bd22e025288063d1",
  ]
}
