terraform {
  required_providers {
    fortimanager = {
      source = "fortinetdev/fortimanager"
      version = "1.3.4"
    }
  }
}

variable "presession" {
  type =  string
  default = ""
}
variable "adom_name" {
  type =  string
  //default = "terraform"
}


provider "fortimanager" {
  hostname     = "139.0.19.10:8443"
  username     = "Adi.Pradana"
  password     = "Aplikas2021!"
  insecure     = "true"

  scopetype    = "adom"
  adom         = "${var.adom_name}"
  logsession = true
  presession = "${var.presession}"
}


resource "fortimanager_exec_workspace_action" "unlockres" {
  scopetype      = "inherit"
  action         = "lockend"
  target         = ""
  param          = ""
  force_recreate = uuid()
  comment        = ""

}