
variable "hostname" {
  type =  string
  default = ""
}

variable "username" {
  type =  string
  default = ""
}

variable "password" {
  type =  string
  default = ""
}

variable "presession" {
  type =  string
  default = ""
}

variable "adom_name" {
  type =  string
  default = "terraform"
}


